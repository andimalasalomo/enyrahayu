-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 30, 2018 at 01:58 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `andi`
--

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `nim` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `kelas` varchar(255) NOT NULL,
  `alamat` varchar(2555) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`nim`, `nama`, `kelas`, `alamat`) VALUES
('11114083', 'Andimala Salomo', '4ka26', 'jakarta'),
('11114084', 'Andimala Salomo', '4ka26', 'jakarta'),
('11114085', 'Andi Salomo Salomo', '4ka26 4ka26', 'jakarta'),
('11114086', 'Andi Salomo Salomo', '4ka27', 'bekasi'),
('11114087', 'Andi Salomo Salomo', '4ka27 4ka27', 'bekasi bekasi'),
('13114585', 'Eny Rahayu', '4ka28', 'padang'),
('13114586', 'Eny Rahayu', '4ka28', 'padang'),
('13114587', 'Eny Rahayu Rahayu', '4ka29 4ka29', 'padang'),
('13114588', 'Eny Rahayu Rahayu', '4ka30', 'lampung'),
('13114589', 'Eny Rahayu Rahayu', '4ka30 4ka30', 'lampung lampung'),
('BKASDASDASD', 'Abraham', 'B7B 5U0', 'Ap #981-2672 Lectus, Avenue'),
('BSX74GDT9SM', 'Declan', 'L2H 9G1', '524-2653 Proin Av.'),
('CFS90IRP2DI', 'David', 'D3J 2X8', 'Ap #971-7068 Dictum St.'),
('CIA11RIS4SV', 'Theodore', 'Y7Z 6Z0', '134-6785 Fringilla Street'),
('CLN09EKI4RL', 'Vance', 'Z8V 5E7', 'P.O. Box 640, 7021 Id Rd.'),
('CPO50EQC4UB', 'Ulric', 'D0Y 1Z3', 'Ap #895-4315 Vitae Avenue'),
('DZB25VGK0PR', 'Knox', 'S7V 3F5', 'P.O. Box 340, 7055 Magna, Av.'),
('FVT52FYL5XS', 'Davis', 'O6K 2L3', '316-1239 Quis Av.'),
('FYB44ORY0EX', 'Hall', 'Z1H 5L3', 'P.O. Box 982, 6939 Luctus Road'),
('GGE06TGI6PO', 'Oliver', 'W6S 2F9', '931-8824 Facilisis Rd.'),
('GMF07TJA0FY', 'Orson', 'Y5B 7O8', '818-5934 Eget Road'),
('GNX97GRN7WW', 'Byron', 'H5J 5W9', '7205 Sodales Road'),
('GRP90RLU8GC', 'Aaron', 'S5G 4B3', '448-5913 Sed Av.'),
('GRV15TET7UP', 'Keith', 'C4I 9B8', '258-6139 Pharetra Street'),
('GXK63BZT9SQ', 'Ezekiel', 'O8S 7I8', 'P.O. Box 409, 9597 Proin Ave'),
('HIY51JIV6IT', 'Leroy', 'Y8X 4F7', '9812 Sodales. Rd.'),
('HKV66YVW7PG', 'Tad', 'G5V 2W0', '3528 Donec Avenue'),
('HXM36CKS1SK', 'Quinn', 'E4F 1M6', '9531 Dui St.'),
('IDD35DVU0QC', 'Jonah', 'N9V 0C8', '568-6485 Suspendisse Rd.'),
('IHZ28XRV3FF', 'Buckminster', 'P0J 0N9', '4423 Et Av.'),
('IQI20YOK4TB', 'Tyrone', 'L8F 5R2', 'Ap #548-8453 Adipiscing Av.'),
('IRC50AQF3OD', 'Jack', 'S7Z 5J6', 'P.O. Box 660, 1785 A Ave'),
('IXO59TSG2XH', 'John', 'N4W 2D1', '566-8401 Nulla. Rd.'),
('JIU90MUA2ZV', 'Elvis', 'G7L 3A8', 'P.O. Box 819, 308 Vitae Av.'),
('JLY47YTQ7TG', 'Dominic', 'D8N 2F0', '4651 Venenatis Ave'),
('JVS30SWT5DW', 'Kenneth', 'S3P 4S2', '5129 Lobortis, Ave'),
('JXM38HGK3IJ', 'Macaulay', 'J4Q 0M3', '248-971 Dolor. Ave'),
('KAW45DNS7ZL', 'Hashim', 'V0U 1Z2', 'P.O. Box 474, 6649 Pede. Rd.'),
('KIK36MFT7HD', 'Judah', 'P0D 4E0', 'Ap #201-5757 Dui. Street'),
('KNS89JHW3LY', 'Jerome', 'B5X 9W1', '3932 Egestas, Av.'),
('KOU22GJT5YU', 'Elliott', 'A5F 5P4', 'Ap #366-9129 Pellentesque Av.'),
('KPE34LNQ6CC', 'Martin', 'X9N 7Y9', 'Ap #666-4352 Sollicitudin St.'),
('KQF77QIZ2NY', 'Flynn', 'Y4R 9C6', '436-6107 Nullam St.'),
('KSP49OFN9CQ', 'Steven', 'O0I 3K5', 'P.O. Box 152, 8908 Aliquam Street'),
('KZY92UAL4UB', 'Lester', 'F6Z 7U5', '812-9189 Donec St.'),
('LBO34OCJ3ON', 'Roth', 'D8E 4L4', 'P.O. Box 267, 3629 Fusce St.'),
('LRV95CWR3DB', 'Gannon', 'U5D 2W2', 'Ap #664-3897 Pede. Road'),
('MHS27ZSA4NA', 'Patrick', 'F5T 3S5', 'Ap #774-696 Sed Av.'),
('MJL43DQJ6DG', 'Gavin', 'V7C 0L8', 'P.O. Box 945, 7918 Ornare, St.'),
('MJP89TFE9LU', 'Chandler', 'E1P 2N4', '752-5043 Id, Street'),
('MKK23NPN7OB', 'Cameron', 'A0E 9T6', '896-5355 Aliquet, Ave'),
('MQI23ORI0UP', 'Bert', 'P8Z 0N6', '487-9678 Vitae Avenue'),
('MSA18BGN9CF', 'Gage', 'F0P 2U1', 'Ap #852-2028 Est. St.'),
('NDY86WYY6DV', 'Dennis', 'K8N 4Y9', 'P.O. Box 594, 5545 Donec Rd.'),
('NLW01JZS5TE', 'Murphy', 'E2P 2I3', '9136 Eu St.'),
('NUY93QXF9WN', 'Randall', 'Z1R 3L6', 'Ap #398-6893 Sapien, Avenue'),
('NXD97TLB3FS', 'Henry', 'R4M 7Q2', '259-2793 Orci Ave'),
('ODR70HCS2TL', 'Henry', 'R2K 3U0', 'P.O. Box 216, 7387 A Ave'),
('OJC46SMW4RX', 'Rooney', 'K7W 9M0', 'P.O. Box 386, 3185 Pellentesque Rd.'),
('OOA79TUV0NZ', 'William', 'E3S 5A6', 'P.O. Box 799, 518 Molestie. Rd.'),
('OOC07EXM9CS', 'Rogan', 'G4I 2F9', '311-1808 Augue Street'),
('OWT91NLU9UP', 'Alden', 'N6P 1J4', 'Ap #394-1338 Aliquam Road'),
('PMQ44OTV8DP', 'Derek', 'O1K 3X8', '4402 Lacus Rd.'),
('PZW72GWF9VL', 'Zane', 'I7A 1X2', '722-1613 Rutrum Avenue'),
('QAL35MDP4GV', 'Austin', 'G9H 7R3', 'P.O. Box 267, 7535 Cursus. Ave'),
('QAN25DUI8KZ', 'Honorato', 'I2W 8X3', '8792 Cras Street'),
('QWY05UXP0CW', 'Ali', 'I3N 8A4', 'Ap #144-2655 Donec St.'),
('RBM56QCS6AR', 'Levi', 'B3R 6N6', '475-4803 Id Rd.'),
('RCU58PDP8ZM', 'Guy', 'Q2M 8D6', 'P.O. Box 830, 608 Congue Rd.'),
('REY16ONA9WD', 'Odysseus', 'G7T 3Q5', 'Ap #802-7848 At Rd.'),
('RHH83UUP7QA', 'Kato', 'X7F 4Y2', 'P.O. Box 718, 4560 Blandit. Street'),
('RZH77CRW4ZH', 'Porter', 'J8I 3A1', '285-5979 Sodales. St.'),
('TCT16TOW5SH', 'Jared', 'W7K 8F1', 'P.O. Box 795, 2168 Suspendisse Rd.'),
('TIC97EYD4BI', 'Wyatt', 'H2O 6L3', 'Ap #949-9954 Tempus Rd.'),
('TSH09NBC2GX', 'Tyrone', 'Q4P 1M1', '261-9373 Convallis St.'),
('TVX95UET3DC', 'Dillon', 'G1H 2E4', 'P.O. Box 111, 5717 Sem Road'),
('TWR19HCV0LK', 'Garrett', 'Q8M 9O8', '432-1412 Integer Rd.'),
('TXD31XFX8KT', 'Malachi', 'P3E 7H6', '1977 Sit Rd.'),
('UAZ17TTQ8PL', 'Chaim', 'D5U 4Z7', 'P.O. Box 608, 9176 Orci Road'),
('UEQ40QJS1TM', 'Alexander', 'A2D 7V5', 'Ap #552-2865 Lacus. Avenue'),
('URW84UMS1BY', 'Griffin', 'X0N 9I2', '950-9870 A, Avenue'),
('UYT97XJN9OD', 'Michael', 'O8F 3I6', 'Ap #952-2197 Dolor Street'),
('VDX61OSF6PM', 'Ferdinand', 'T7C 5P3', '729-9580 Vitae Road'),
('VLY93XVZ1CZ', 'Callum', 'J0V 3O0', '9348 Nunc St.'),
('VZP47DBO9GK', 'Tanner', 'I0T 0W0', '1018 Arcu St.'),
('WBP67JXL2ZF', 'Aladdin', 'P0F 1P7', '9410 Sociis Road'),
('WIE10OUQ7GJ', 'Alfonso', 'U0L 7S0', 'P.O. Box 888, 6703 Mattis Av.'),
('WZC25ATG5NA', 'Gary', 'K3Q 7O0', '6639 Odio. Rd.'),
('XPG45DVV5XO', 'Kato', 'C7M 5X6', 'P.O. Box 107, 9610 Habitant Street'),
('XPY17VXK0II', 'Stephen', 'L8Q 2K2', '5041 Nec St.'),
('XVE23XBJ6KQ', 'Tucker', 'I0F 8A6', 'Ap #116-6424 Faucibus Av.'),
('XZM28DCE0FT', 'Salvador', 'E7Q 7A9', '5169 Rhoncus Road'),
('YAD76NSB8XL', 'Timothy', 'G7J 1F2', '688-2064 Sodales Av.'),
('YIC39KCN8WB', 'Jermaine', 'Y6I 8J4', 'Ap #119-9163 Massa. Avenue'),
('YUW67NMI8WZ', 'Cain', 'I5M 1M6', 'Ap #903-308 Dictum Avenue'),
('YWC32KTX8MM', 'Kenyon', 'F9E 3X7', '796-5807 Sagittis Rd.'),
('YXQ91WIY3WB', 'Amal', 'N0O 5N0', 'P.O. Box 668, 1686 Suscipit Rd.'),
('ZCP10AHD5FX', 'Harding', 'T1Q 1K1', 'P.O. Box 221, 9576 Convallis St.'),
('ZVL25FJT1KH', 'Boris', 'V3F 1A1', 'Ap #439-2916 Elit. St.'),
('ZXI72POC6SL', 'Cameron', 'F2A 5Q1', 'P.O. Box 545, 8592 Nunc Road');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`nim`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
