    <!-- SLIDER -->
    <div class="slider d-md-flex align-items-center">
        <!-- <img src="images/slider.jpg" class="img-fluid" alt="#"> -->
        <div class="container">
            <div class="row  justify-content-center">
                <div class="col-md-12">
                    <div class="slider-title_box">
                        <div class="row justify-content-center">
                            <div class="col-md-10">
                                <form action = "<?php echo base_url("user/searchjaccard"); ?>" method="post" class="form-wrap">
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <input type="text" name="search" placeholder="<?php if(isset($search)){ echo $search; }else{echo 'Masukkan data yang anda inginkan';}?> " class="btn-group1 w-100">
                                        <button type="submit" class="btn-form"><span class="icon-magnifier search-icon"></span>SEARCH<i class="pe-7s-angle-right"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php 
      if($view==""){ } 
      elseif (isset($eror)) {?>
        <div class="align-items-center mb-5 table-cs">

              <h3 style="color: red;text-align:center;"">Maaf hasil pencarian tidak ditemukan</h3></center>
        </div>
      <?php }
      else{ 
     ?>
    <div class="align-items-center mb-5 table-cs">
    <div class="container"> 
    <div class="row"> 
    
    <table class="table-cs table table-striped ">
    <thead>
      <tr>
        <th scope="col">no</th>
        <th scope="col">nim</th>
        <th scope="col">nama</th>
        <th scope="col">kelas</th>
        <th scope="col">alamat</th>
        <th scope="col">jumlah</th>
      </tr>
    </thead>
    <tbody>
      <?php 
      $no = 1;
       foreach ($view as $data) {
        $nim = $data['nim'];
        $nama = $data['nama'];
        $kelas = $data['kelas'];
        $alamat = $data['alamat'];
        $jumlah = $data['jumlah'];
      ?>
        <tr>
          <td> <?php echo $no++ ?> </td>
          <td> <?php echo $nim ?> </td>
          <td> <?php echo $nama ?> </td>
          <td> <?php echo $kelas ?> </td>
          <td> <?php echo $alamat ?> </td>
          <td> <?php echo $jumlah ?> </td>
        </tr>
      <?php 
        /*if ($no > 10) {
          break;
        }*/
      } } ?>
      
    </tbody>
</table>
</div>  
 </div> 
    </div>
