<body>
    <!--============================= HEADER =============================-->
<?php $curr_url = uri_string(); ?>
    <div class="nav-menu">
        <div class="bg transition">
            <div class="container-fluid ">
                <div class="row">
                    <div class="col-md-12">
                        <nav class="navbar navbar-expand-lg navbar-light">
                            <a class="<?php if($curr_url == "user/index"){ echo "active";}?> navbar-brand" href="<?php echo base_url("user/index");?>">Home</a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="icon-menu"></span>
                            </button>

                            <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                                <ul class="navbar-nav mr-auto">
                                    <li class="nav-item">
                                        <a class="<?php if($curr_url == "user/cosine" or $curr_url == "user/searchcosine"){ echo "active";}?> nav-link" href="<?php echo base_url("user/cosine");?>">Cosine</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="<?php if($curr_url == "user/jaccard" or $curr_url == "user/searchjaccard"){ echo "active";}?> nav-link" href="<?php echo base_url("user/jaccard");?>">Jaccard</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
