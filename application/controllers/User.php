<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
      function __construct(){
        parent::__construct();
        $this->load->model('M_global');
    }
	function index(){
     $this->load->view('user/skeleton/skeleton_header');
        $this->load->view('user/skeleton/skeleton_navbar');
        $this->load->view('user/index');        
        $this->load->view('user/skeleton/skeleton_footer');

    }   
    function cosine(){
        $data['view']='';
        $this->load->view('user/skeleton/skeleton_header');
        $this->load->view('user/skeleton/skeleton_navbar');
        $this->load->view('user/pages/cosine',$data);        
        $this->load->view('user/skeleton/skeleton_footer');
    }    
     function jaccard(){
        $data['view']='';
        $this->load->view('user/skeleton/skeleton_header');
        $this->load->view('user/skeleton/skeleton_navbar');
        $this->load->view('user/pages/jaccard',$data);        
        $this->load->view('user/skeleton/skeleton_footer');
    }  

    public function compareStringsCosine($str1, $str2)
    {
        $a = strtoupper($str1);
        $b = strtoupper($str2);

        $word1 = array_unique(explode(' ', $a)); 
        $word2 = array_unique(explode(' ', $b)); 
        
        $pairs1 = $word1;
        $pairs2 = $word2;

        $intersection = 0;

        $count1 = count($word1);
        $count2 = count($word2);

        $union = sqrt($count1) * sqrt($count2);
        $pairs1 = array_values($pairs1);
        $pairs2 = array_values($pairs2);
        for ($i=0; $i < count($pairs1); $i++)
        {
            $pair1 = $pairs1[$i];

            $pairs2 = array_values($pairs2);
            for($j = 0; $j < count($pairs2); $j++)
            {
                $pair2 = $pairs2[$j];
                if ($pair1 === $pair2)
                {
                    $intersection++;
                    unset($pairs2[$j]);
                    break;
                }
            }
        }
      return (($intersection)/$union)*100;
    }

    public function compareStringsJaccard($str1, $str2)
    {
        $a = strtoupper($str1);
        $b = strtoupper($str2);

        $word1 = array_unique(explode(' ', $a)); 
        $word2 = array_unique(explode(' ', $b)); 
        
        $pairs1 = $word1;
        $pairs2 = $word2;

        $intersection = 0;

        $union = count($word1) + count($word2);
        $pairs1 = array_values($pairs1);
        $pairs2 = array_values($pairs2);
        for ($i=0; $i < count($pairs1); $i++)
        {
            $pair1 = $pairs1[$i];

            $pairs2 = array_values($pairs2);
            for($j = 0; $j < count($pairs2); $j++)
            {
                $pair2 = $pairs2[$j];
                if ($pair1 === $pair2)
                {
                    $intersection++;
                    unset($pairs2[$j]);
                    break;
                }
            }
        }
    return (($intersection)/($union-$intersection))*100;
    }

    function searchcosine(){
        $getcosine = $this->M_global->get('test','','*');
        $inputcosine = $this->input->post('search');
        $i=0;
        $data_hasil = "";
        foreach ($getcosine as $key) {
            $nim = $key->nim;
            $nama = $key->nama;
            $kelas = $key->kelas;
            $alamat = $key->alamat;
            $string = $nim.' '.$nama.' '.$alamat.' '.$kelas.' '.$alamat;
            $hasil = $this->compareStringsCosine($inputcosine, $string);
            if ($hasil > 0) {
                $data_hasil[$i] = array(
                    'nim' => $nim,
                    'nama' => $nama,
                    'kelas' => $kelas,
                    'alamat' => $alamat,
                    'jumlah' => $hasil,
                );
                $i++;
            }
        }

            if ($data_hasil == null) {
                $data['eror'] = "ga ada";
                $data['view'] = "ga ada";
            }else{
                // Obtain a list of columns
                foreach ($data_hasil as $key => $row) {
                    $jumlah[$key]  = $row['jumlah'];
                }

                // as of PHP 5.5.0 you can use array_column() instead of the above code
                $jumlah  = array_column($data_hasil, 'jumlah');

                // Sort the data with volume descending, edition ascending
                // Add $data as the last parameter, to sort by the common key
                array_multisort($jumlah, SORT_DESC, $data_hasil);
                $data['view'] = $data_hasil;
            }
        $data['search'] = $inputcosine;
        $this->load->view('user/skeleton/skeleton_header');
        $this->load->view('user/skeleton/skeleton_navbar');
        $this->load->view('user/pages/cosine',$data);        
        $this->load->view('user/skeleton/skeleton_footer');

    }       

    function searchjaccard(){
        $getjaccard = $this->M_global->get('test','','*');
        $inputjaccard = $this->input->post('search');
        $i=0;
        $data_hasil = "";
        foreach ($getjaccard as $key) {
            $nim = $key->nim;
            $nama = $key->nama;
            $kelas = $key->kelas;
            $alamat = $key->alamat;
            $string = $nim.' '.$nama.' '.$alamat.' '.$kelas.' '.$alamat;
            $hasil = $this->compareStringsJaccard($inputjaccard, $string);
            if ($hasil > 0) {
                $data_hasil[$i] = array(
                    'nim' => $nim,
                    'nama' => $nama,
                    'kelas' => $kelas,
                    'alamat' => $alamat,
                    'jumlah' => $hasil,
                );
                $i++;
            }
        }

            if ($data_hasil == null) {
                $data['eror'] = "ga ada";
                $data['view'] = "ga ada";
            }else{
                // Obtain a list of columns
                foreach ($data_hasil as $key => $row) {
                    $jumlah[$key]  = $row['jumlah'];
                }

                // as of PHP 5.5.0 you can use array_column() instead of the above code
                $jumlah  = array_column($data_hasil, 'jumlah');

                // Sort the data with volume descending, edition ascending
                // Add $data as the last parameter, to sort by the common key
                array_multisort($jumlah, SORT_DESC, $data_hasil);
                $data['view'] = $data_hasil;
            }
        $data['search'] = $inputjaccard;
        $this->load->view('user/skeleton/skeleton_header');
        $this->load->view('user/skeleton/skeleton_navbar');
        $this->load->view('user/pages/jaccard',$data);        
        $this->load->view('user/skeleton/skeleton_footer');

    }  
}